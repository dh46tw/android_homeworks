package tw.dh46.currency;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edNtd;
    private Button btnExcute;
    private TextView tvUsCurrency, tvJpCurrency;

    float usCurrency = 30.9f;
    float jpCurrency = 0.285f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        edNtd = findViewById(R.id.ed_ntd);
        btnExcute = findViewById(R.id.btn_excute);
        tvJpCurrency = findViewById(R.id.tv_jp);
        tvUsCurrency = findViewById(R.id.tv_us);

        tvUsCurrency.setText(usCurrency + "");
        tvJpCurrency.setText(jpCurrency + "");

        btnExcute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeMyTask();
            }
        });
    }

    private float calculate(String input, float currency) {
        float inputNtd = Float.parseFloat(input);
        return inputNtd / currency;
    }

    private void executeMyTask() {
        String input = edNtd.getText().toString();
        String title = "Error";
        String msg = "Please enter your NTD amount.";
        if (!input.equals("")) {
            title = "Result";
            msg = "USD is " + calculate(input, usCurrency) + "\n"
                    + "JPY is " + calculate(input, jpCurrency) + "\n";

            //            new AlertDialog.Builder(MainActivity.this)
//                    .setTitle("Result")
//                    .setMessage(msg)
//                    .setCancelable(false)
//                    .setPositiveButton("OK", null)
//                    .show();
        }
//        else {
//            Toast.makeText(MainActivity.this, "Please enter your NTD amount.",Toast.LENGTH_SHORT).show();
//            new AlertDialog.Builder(MainActivity.this)
//                    .setTitle("Error")
//                    .setMessage("Please enter your NTD amount.")
//                    .setPositiveButton("OK",null)
//                    .show();
//        }
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .show();
    }
}
